import http, {IncomingMessage, ServerResponse} from 'http';
import {parse} from 'url';
import fetch from 'node-fetch';
import fs from'fs';

const BACKEND = 'http://localhost:10080/beta1/articles/';
const BACKENDFILES='http://localhost:10080/beta1/files/mypath.jpg';

const testData = {
	"_id": "ad880bf7-52e6-43cc-8906-18e31350da5f",
	"name": {
	  "ja_JP": "安全衛生業務"
	},
	"body": {
	  "settings": {
		"notification": true,
		"notificationType": "mail"
	  },
	  "items": [
		{
		  "itemId": "7d40c763-babf-4a77-b8cf-23f8e2d6c39a",
		  "name": {
			"ja_JP": "写真"
		  },
		  "order": 1,
		  "type": "addPicture",
		  "options": {
			"maxPhotos": 1
		  },
		  "required": false,
		  "status": "available"
		},
		{
		  "itemId": "403cc9b0-f665-42d4-bec1-f52241deb4f2",
		  "name": {
			"ja_JP": "場所"
		  },
		  "order": 2,
		  "type": "spinner",
		  "options": {
			"choices": []
		  },
		  "required": false,
		  "status": "available"
		},
		{
		  "itemId": "0ed25f10-7317-410e-89df-cb731306cb77",
		  "name": {
			"ja_JP": "投稿者"
		  },
		  "order": 3,
		  "type": "user",
		  "required": false,
		  "status": "available"
		},
		{
		  "itemId": "587ead87-fbf2-4f67-9867-ec801f2f5dda",
		  "name": {
			"ja_JP": "指摘事項"
		  },
		  "order": 4,
		  "type": "textInput",
		  "required": false,
		  "status": "available"
		},
		{
		  "id": "af1ff43c-e9ef-4d65-8c48-1fff4d8f4ce4",
		  "name": {
			"ja_JP": "グッドポイント"
		  },
		  "order": 5,
		  "type": "checkbox",
		  "required": false,
		  "status": "available"
		}
	  ]
	}
  };

const postJson = async (url: string, body: any): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {'Content-Type': 'application/json',
					'connection': 'keep-alive'}
		});
//		console.log(JSON.stringify(body));
		const cnt = await response.buffer();
//		console.log(cnt.toString());
		const rc: number = await response.status;
//		console.log(rc);
		return [rc, cnt];
	} catch (error) {
		console.log(error);
		return [404, ""];
	}
};

const getJson = async (url: string): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'GET',
			headers: {'connection': 'keep-alive'}
		});
		const body = await response.buffer();
		const rc: number = await response.status;
		console.log(body.toString());
		return [rc, body];
	} catch (error) {
		console.error(error);
		return [404, ""];
	}
};

async function main() {
	const [rc0, cont0] = await postJson(BACKENDFILES, testData);
	if (typeof(cont0) == 'number') {
		console.log('error');
	}
	console.log('POST');
	console.log(rc0);
	console.log(cont0.toString());
	
//	const data = fs.readFileSync('/home/hides/Pictures/test.jpg');
//	const [rc, cont] = await postJson(BACKENDFILES, data);
//	if (typeof(cont) == 'number') {
//		console.log('error');
//	}
//	console.log('POST');
//	console.log(rc);
//	console.log(cont.toString());
/*
	const [rcg, contg] = await getJson(BACKEND);
	if (typeof(contg) == 'number') {
		console.log('error');
	}
	console.log('GET');
	console.log(rcg);
	console.log(contg.toString());
*/
}

main();

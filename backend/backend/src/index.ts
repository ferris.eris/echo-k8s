import http, {IncomingMessage, ServerResponse} from 'http';
import {parse} from 'url';
import { rejects } from 'assert';

// リクエスト情報を取得する
const getInformations = function(request: IncomingMessage){
	let url = parse(request.url || "", true);
	console.log(url);
	console.log(request.headers);
	return {
		'アドレス情報':{
			'アドレス': url.href,
			'プロトコル': url.protocol,
			'ホスト名': url.hostname,
			'パス': url.path,
//			'クエリ': url.query
		},
		'リクエスト情報':{
			'データ送信':request.method,
			'ホスト（ヘッダー情報）':request.headers['host'],
			'コネクション（ヘッダー情報）':request.headers['connection'],
			'キャッシュコントロール（ヘッダー情報）':request.headers['cache-control'],
			'アクセプト（ヘッダー情報）':request.headers['accept'],
			'アップグレードリクエスト（ヘッダー情報）':request.headers['upgrade-insecure-requests'],
			'ユーザーエージェント（ヘッダー情報）':request.headers['user-agent'],
			'エンコード（ヘッダー情報）':request.headers['accept-encoding'],
			'言語（ヘッダー情報）':request.headers['accept-language'],
			'オーソリゼーション':request.headers['authorization'],
		}
	};
};

// オブジェクトをhtmlリスト形式に書き出す
const setInformations = function(dataObject: any){
	let keys = Object.keys(dataObject);
	let returnText = '';
	for(let k in keys){
        const key = keys[k];
		if(dataObject[key] instanceof Object){
			returnText += '<h1>' + key + '</h1>\n' + '<dl>\n' + setInformations(dataObject[key]) + '</dl>\n';
		}
		else{
			returnText += '  <dt>' + key + '</dt>\n    <dd>' + dataObject[key] + '</dd>\n';
		}
	}
	return returnText ;
}

const server = http.createServer();

server.on('request', function(request, response) {
	let body: any = [];
	let bodyStr: string = "";
	let sss: number = 0;
	request.on('data', (chunk: any) => {
		body.push(chunk);
		sss += chunk.length;
	}).on('end', () => {
		console.log(body);
		bodyStr = Buffer.concat(body).toString();
		console.log(bodyStr);
		console.log("end");
		response.writeHead(200, {'content-Type':'text/html; charset=UTF-8'});
		const requestData = getInformations(request);
	//    console.log(requestData);
		const htmlText = setInformations(requestData);
		response.write(htmlText + sss.toString());
		response.end();
		}).on('error', (e: any) => {
		console.error('error');
	});

})
server.listen(3000); // 3000番ポートで起動
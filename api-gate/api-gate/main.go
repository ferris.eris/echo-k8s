package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
)

type apiGate struct {
	API         string `json:"api"`
	Backend     string `json:"backend"`
	BackendHost string `json:"backendHost"`
}

func main() {
	jsonFile := os.Getenv("API_GATE_JSON")
	if jsonFile == "" {
		jsonFile = "api-gate.json"
	}
	port := os.Getenv("API_GATE_PORT")
	if port == "" {
		port = "10080"
	}
	bytes, err := ioutil.ReadFile(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	var gates []apiGate
	if err := json.Unmarshal(bytes, &gates); err != nil {
		log.Fatal(err)
	}

	director := func(req *http.Request) {
		for _, gate := range gates {
			if strings.HasPrefix(req.URL.Path, gate.API) {
				origin, _ := url.Parse(gate.BackendHost)
				req.URL.Host = origin.Host
				req.URL.Path = strings.Replace(req.URL.Path, gate.API, gate.Backend, 1)
				req.URL.Scheme = "http"
				req.Header.Add("X-Forwarded-Host", req.Host)
				req.Header.Add("X-Origin-Host", origin.Host)
				return
			}
		}
		req.URL.Scheme = "http"
		log.Printf("director: %s\n", req.URL)
	}

	modifier := func(res *http.Response) error {
		res.Header.Set("Access-Control-Allow-Origin", "*")
		return nil
	}

	proxy := &httputil.ReverseProxy{
		Director:       director,
		ModifyResponse: modifier,
	}

	log.Printf("port = %s\n", port)
	pos := strings.LastIndex(port, ":")
	if pos < 0 {
		port = "0.0.0.0:" + port
	} else {
		port = "0.0.0.0" + port[pos:]
	}
	log.Printf("port = %s\n", port)
	server := http.Server{
		Addr:    port,
		Handler: proxy,
	}
	//	server.Addr = "0.0.0.0:10080"

	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err.Error())
	}
	/*
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			proxy.ServeHTTP(w, r)
		})
		log.Fatal(http.ListenAndServe("0.0.0.0:10080", nil))
	*/
}

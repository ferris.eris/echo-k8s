# Kubernetes上でのとあるプロジェクト向け試作

## 概要

```
ユーザ
↓ HTTPS
Ingress
↓ HTTP(80)
↓
echo-k8sサービス
↓
Pod
nginx（リバースプロキシ）
/api /
↓   ↓8081 ↓8082 ↓8083 
↓   p001   p002   p003
↓ HTTP(80)
↓
api-gateサービス
Pod
nginx
HTTP(10080)
api-gate
↓
↓ HTTP(3000)
↓
backend-adapterサービス
Pod
backend
```

※Snap（Snappy）で提供されているMicrok8sにて動作確認
※echo-k8sサービスとapi-gateサービスの上にはNodePortサービスが動作している

## 実行コマンド

start.shで実行開始
end.shで実行終了
ingressを動作させる場合はstart.sh実行後以下に従う

```
# Secret作成
kubectl create secret tls tls-certificate --key oreore.key --cert oreore.crt

# Ingressの適用
kubectl apply -f ingress.yaml
```

p001、p002、p003、main、api-gate、backendの各コンテナはGitLabのレジストリのものを使用する（GitHubのリソースを使いたくないため）<br>
GitLab側からCIできるように`.gitlab-ci.yml`ファイルを保持する

現時点でのbackendはtext/htmlを返すためAPI実行はエラーとなる

以下のように変換している（JPEGの転送はデバッグしていない）

- api-gateは複数形だがbackend-adapterは単数形
- api-gateは`-`（ハイフン）で単語を接続するが、backend-adapterは`_`（アンダースコア）で単語を接続

| Application → api-gate | api-gate → backend-adapter |
|-------------------------|-----------------------------|
| /beta1/articles/        | /backend-adapter:3000/beta1/article              |
| /beta1/article-templates | /backend-adapter:3000/beta1/article_template    |
| /beta1/aritcle-sample-templates | /backend-adapter:3000/beta1/article_sample_template |
| /beta1/files/<`path`>   | /backend-adapter:3001/beta1/<`path`> |

今後

- **namespaceを定義する**
- api-gateはAPIの種類に応じてコンテナに分けるべき
- api-gateのindex.ts
    - 決め打ちな変数・定数が存在するため、環境変数化する
    - 現在サーバ機能には、httpライブラリを使用しているが、expressを使うか検討する

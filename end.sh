kubectl delete -f k8s/echo-k8s-deployment.yaml
kubectl delete -f k8s/nginx-config.yaml
kubectl delete -f k8s/api-gate-deployment.yaml
kubectl delete -f k8s/api-gate-config.yaml
kubectl delete -f k8s/backend-deployment.yaml
kubectl get svc
kubectl get pods
